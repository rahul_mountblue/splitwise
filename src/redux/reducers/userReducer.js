import { actionTypes } from "../constant/action-type";

const initialState = {
    userData: {},
    login: false,
    freinds: [],
    expensesData:[],

}

export const userReducer = (state = initialState, { type, payload }) => {

    switch (type) {
        case actionTypes.SET_USERDATA:
            return { ...state, userData: payload};

        case actionTypes.LOG_OUT:
            return {
                ...state, login: false
            }
        case actionTypes.ADD_FRIEND:


            return {
                ...state, freinds: [...state.freinds, payload]
            }
        case actionTypes.ADD_EXPENSES:

            return {
              ...state,expensesData:[...state.expensesData,payload]
            }
        case actionTypes.LOG_IN:

        return {
            ...state,login:payload
        }

        default:
            return state;

    }
};