import { actionTypes } from '../constant/action-type';

export const userData = (data) => {

    return {
        type: actionTypes.SET_USERDATA,
        payload: data,
    }
}

export const logOut = (data) => {

    return {
        type: actionTypes. LOG_OUT,
        payload: data,
    }
}
export const addFriend = (data)=>{
     return {
         type:actionTypes.ADD_FRIEND,
         payload:data,
     }  
}

export const addExpenses = (data)=>{
    return {
        type:actionTypes.ADD_EXPENSES,
        payload:data,
    }  
}
export const login = (data)=>{
    return {
        type:actionTypes.LOG_IN,
        payload:data
    }
}





