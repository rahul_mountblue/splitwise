import React from "react";
import logo from "../image/splitwiseIcon.svg";
import { Link } from "react-router-dom";
import avatar from "../image/avatar-grey.jpeg";
import Header from './Header';

export default class Addgroup extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      count: 0,
      value: [],
      flag: false,
    };
  }

  handle = () => {
    this.setState({
      flag: true,
    });
  };
  add = () => {
    this.state.value.push(this.state.count);
    this.setState({
      count: this.state.count + 1,
    });
  };

  delete = () => {
    let arr = this.state.value;
    arr.splice(0,1);
    this.setState({
      value: arr,
    })
  };
  render() {
    return (
      <>
      <Header />

        <div className="container" style={{ marginTop: "5%" }}>
          <div className="d-flex flex-row m-auto w-70 h-60">
            <div className="px-5">
              <img src={logo} alt="" className="img-fluid img-logo" />
            </div>
            <div>
              <small className="light-grey d-block  h3">
                START A NEW GROUP
              </small>
              <h4 className="h5">My group shall be called…</h4>
              <input
                className="w-100 p-3 input"
                placeholder="Group name"
                onChange={this.handle}
              />
              <hr />
              {this.state.flag ? (
                <div>
                  <h4 className="light-grey text-secondary mb-2">GROUP MEMBERS</h4>
                  <small className=" lead mt-2 form-text text-muted">
                    Tip: Lots of people to add? Send your friends an{" "}
                    <a>invite link</a>
                  </small>
                  <div>
                    {this.state.value.map((element) => (
                      <div className="row p-2">
                        <div className="col">
                          <img src={avatar} alt="" className="avatar" />
                        </div>
                        <div className="col">
                          <input 
                          type="text"
                          className="input-p"
                          placeholder="name" 
                          />
                        </div>
                        <div className="col">
                          <input type="text" 
                          className="input-p" 
                          placeholder="email" />
                        </div>
                        <div className="col">
                          <button
                            style={{ border: "0 solid" }}
                            onClick={this.delete}
                          >
                            <i className="fa fa-close mt-1" style={{ color: "red" }}></i>
                          </button>
                        </div>
                      </div>
                    ))}
                    <div>
                      <button onClick={this.add} style={{ border: "0 solid" }} className="mt-2">
                       <a className="text-blue "> +add Person</a>
                      </button>
                    </div>
                    <div>
                      <h4 className="light-grey mt-2 mb-2 text-secondary">GROUP Type</h4>
                      <select>
                        <option>HOME</option>
                        <option>TRIP</option>
                        <option>COUPLE</option>
                        <option>OTHER</option>
                      </select>
                      <hr/>
                    </div>
                    <div className="p-2">
                      <a style={{cursor: "pointer"}} className="text-blue">Advance settings</a>
                    </div>
                  </div>
                </div>
              ) : null}
            </div>
          </div>
          <div style={{marginLeft: "30%"}}>
              <button className="btn btn-info text-light">SAVE</button>
            </div>
        </div>
      </>
    );
  }
}
