import React, { Component } from 'react';
import Dashboard from './Dashboard';
import { connect } from 'react-redux';
import { userData, addFriend } from '../redux/action/userActions';
import Allexpenses from './Allexpenses';
import { Modal, Button } from 'react-bootstrap';
import RecentActivity from './RecentActivity';

const mapStateToProps = (props) => {
    return {
        user: props.allUser,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        userData: (data) => dispatch(userData(data)),
        addFriend: (data) => dispatch(addFriend(data)),

    }
}

class Main extends Component {
    constructor(props) {
        super(props)
        this.state = {

            showDashBoard: true,
            showActivity: false,
            showAddExpenses: false,
            show: false,
            name: '',
        }
    }

    showDashboardSection = () => {
        this.setState({
            showDashBoard: true,
            showActivity: false,
            showAddExpenses: false,
        })
    }

    showAllExpenses = () => {
        this.setState({
            showDashBoard: false,
            showActivity: false,
            showAddExpenses: true,
        })
    }

    showRecentActivity = () => {
        this.setState({
            showDashBoard: false,
            showActivity: true,
            showAddExpenses: false,
        })
    }

    handleShow = () => {
        this.setState({ show: true });
    }

    handleClose = () => {
        this.setState({ show: false });
    }

    handleChange = (event) => {

        this.setState({ [event.target.name]: event.target.value })

    }

    handleSubmit = () => {
        //   console.log(this.state.name);
        this.props.addFriend(this.state.name);
        this.handleClose();
    }
    handleRedirect = () => {

    }

    render() {
        const { show } = this.state;
        console.log(this.props.user.freinds);
        return <div className='container'>
            <div className="row">
                <div className="col-sm-3">
                    <div className='bg-light shadow p-2 d-flex flex-column justify-content-start'>
                        <div onClick={this.showDashboardSection} className={this.state.showDashBoard ? "p-1 pointer selected" : "p-1 pointer"}>
                            Dashboard
                        </div>
                        <div onClick={this.showRecentActivity}
                            className={this.state.showActivity ?
                                "p-1 pointer selected" :
                                "p-1 pointer"}>
                            Recent Activity
                        </div>
                        <div onClick={this.showAllExpenses}
                            className={this.state.showAddExpenses ?
                                "p-1 pointer selected"
                                :
                                "p-1 pointer"}>
                            All Expenses
                        </div>

                        <div>

                            <div className='bg-light'>
                                <h4 className='p-1 m-0 d-flex flex-row justify-content-between align-items-center'> Groups <span className='pointer text-secondary amount' onClick={this.handleRedirect}>+ add</span></h4>
                            </div>
                            <div className='bg-light'>

                                <h4 className='p-1 m-0 d-flex flex-row justify-content-between align-items-center'> Friends <span className='pointer text-secondary amount ' onClick={this.handleShow}>+ add</span></h4>
                                <ul className='bg-light'>
                                    {this.props.user.login && this.props.user.freinds.length > 0 ?
                                        this.props.user.freinds.map((name) => {
                                            return <li className='bg-white p-1'>
                                                <i className="fa fa-user-circle-o" style={{ fontSize: "15px" }}>
                                                    <span> {name}</span>
                                                </i>

                                            </li>
                                        })
                                        :
                                        undefined
                                    }
                                </ul>
                            </div>

                        </div>


                    </div>
                </div>
                <div className="col-sm-6 border-right-1 border-left-1">
                    {
                        this.state.showDashBoard ?
                            <Dashboard />
                            :
                            undefined
                    }
                    {
                        this.state.showAddExpenses ?
                            <Allexpenses />
                            :
                            undefined
                    }
                    {
                        this.state.showActivity ?
                            <RecentActivity />
                            :
                            undefined
                    }
                </div>
                <div className="col-sm-3">

                </div>
            </div>

            <Modal show={show}>
                <Modal.Header >
                    <Modal.Title>Add friend</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form>
                        <div class="form-group mt-2">

                            <input type="text"
                                name='name'
                                className="form-control"
                                id="exampleInputEmail1"
                                aria-describedby="emailHelp"
                                onChange={this.handleChange}
                                placeholder="Enter name"
                            />
                        </div>

                        <div class="form-group mt-2">

                            <input type="email"
                                name="email"
                                className="form-control"
                                id="exampleInputPassword1"
                                placeholder="Enter email"
                                onChange={this.handleChange}
                            />
                        </div>

                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={this.handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={this.handleSubmit}>
                        Add
                    </Button>
                </Modal.Footer>
            </Modal>

        </div>;
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);
