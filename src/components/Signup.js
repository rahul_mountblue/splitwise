import React, { Component } from 'react';
import { GoogleLogin, GoogleLogout } from 'react-google-login';
import { connect } from 'react-redux';
import { userData } from '../redux/action/userActions';
import Header from './Header';
import Welcome from './Welcome';
import validator from 'validator';
import { Link } from 'react-router-dom'

const mapStateToProps = (props) => {
    return {
        user: props.allUser,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        userData: (data) => dispatch(userData(data)),

    }
}


class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            password: '',
            cpassword: '',
            termsaccepted: false,
            errors: {},
            isSubmit: false,
            showError: true,


        }

    }

    handleChange = (event) => {

        const { name, value } = event.target;
        this.setState({ showError: false })

        if (event.target.type !== 'checkbox') {
            this.setState({ [name]: value });
        } else {
            this.setState({ [name]: event.target.checked })
        }

    }

    handleSubmit = (event) => {
        event.preventDefault();

        let validSubmission = this.formValidation();

        this.setState({
            isSubmit: validSubmission,
            showError: true,
        });
        if (validSubmission) {
            this.props.userData(this.state);
            this.setState({
                name: '',
                email: '',
                password: '',
                cpassword: '',
                termsaccepted: false,
                errors: {},
                isSubmit: true,
                showError: true,
            });

        }
    }


    loginSuccessWithGoogle = (res) => {
        let data = {};
        console.log(res);
        data.name = res.profileObj.name;
        data.email = res.profileObj.email;
        data.image = res.profileObj.imageUrl;
        this.setState({ login: true });
        this.props.userData(data);
    }

    formValidation = () => {
        const { name, email, password, cpassword, termsaccepted } = this.state;
        let isValid = true;

        let errors = {
            nameError: '',
            emailError: '',
            passError: '',
            cpassError: '',
            matchPassword: '',
            termsAcceptedError: ''

        };

        if (validator.isEmpty(name)) {
            errors.nameError = 'Name can not be empty';
        } else if (validator.isAlpha(name, 'en-US', { ignore: ' -' }) === false) {
            errors.nameError = 'Name can not be invalid value';
        } else if (name.length < 3 || name.length > 30) {
            errors.nameError = 'Name should be between 3 to 30 charactore';
        }

        if (validator.isEmpty(email)) {
            errors.emailError = 'Email can not be empty';
        } else if (validator.isEmail(email, { blacklisted_chars: ' ' }) === false) {
            errors.emailError = 'Invalid mail';
        }



        let passwordParam = {
            minLength: 4, maxLength: 10, minLowercase: 1,
            minUppercase: 1, minNumbers: 1,
            minSymbols: 1, returnScore: false
        }

        if (validator.isEmpty(password)) {
            errors.passError = 'Password field can not be empty';
        } else if (validator.isStrongPassword(password, passwordParam) === false) {
            errors.passError = `Need 1 lowercase, 
            1 uppercase alphabet and 1 number and 1 special symbol`;
        }

        if (validator.isEmpty(cpassword)) {
            errors.cpassError = 'Confirm Password field can not be empty';
        } else if (validator.isStrongPassword(cpassword, passwordParam) === false) {
            errors.cpassError = `Need 1 lowercase, 
            1 uppercase alphabet and 1 number and 1 special symbol`;
        }

        if (validator.equals(password, cpassword) === false) {
            errors.matchPassword = "Password not matched"
        }

        if (termsaccepted === false) {
            errors.termsAcceptedError = "Please accept the terms and conditions"
        }


        this.setState({ errors });

        let isError = Object.keys(errors).find(error => {
            return errors[error].length > 0
        })

        if (isError !== undefined) {
            isValid = false;
        }
        return isValid;


    }

    loginFailureWithGoogle = () => {

    }
    render() {
        let clientId = "277972961938-euq76op4cdmbujch98ntt100qsslgja1.apps.googleusercontent.com";
        const { name, email, password, cpassword, errors, isSubmit, showError } = this.state;
        console.log(this.props.user);
        return <div className='container-fluid'>
            <Header />
            {isSubmit ?
                <div className='d-flex justify-content-center'>
                    <div className='p-3  mt-2 w-25'>
                        <p className='text-center text-success '>Signup successfull</p>
                    </div>
                </div>

                :
                undefined
            }
            {
                this.props.user.login ?
                    <Welcome />
                    :

                    <div className='row justify-content-center align-items-center'>
                        <div className='col-sm-6 col-md-4  mt-5 border'>
                            <h1 className='h4  text-center p-2 font-signup'>Sign<span> up</span></h1>
                            <form className='mt-2 mb-3 p-2'>

                                <div className="mb-3">

                                    <input type="text"
                                        name="name"
                                        className="form-control"
                                        id="nameInput"
                                        onChange={this.handleChange}
                                        value={name}
                                        placeholder="Enter name"
                                    />

                                    <p className="text-danger text-center mt-1">
                                        {showError && errors.nameError && errors.nameError.length > 0 ?
                                            errors.nameError
                                            :
                                            undefined
                                        }
                                    </p>



                                </div>

                                <div className="mb-3">

                                    <input type="email"
                                        name="email"
                                        className="form-control"
                                        id="emailInput"
                                        onChange={this.handleChange}
                                        value={email}
                                        placeholder="Enter email"
                                    />
                                    <p className="text-danger text-center mt-1">
                                        {showError && errors.emailError && errors.emailError.length > 0 ?
                                            errors.emailError
                                            :
                                            undefined
                                        }
                                    </p>
                                </div>


                                <div className="mb-3">

                                    <input type="password"
                                        name="password"
                                        className="form-control"
                                        id="passInput"
                                        onChange={this.handleChange}
                                        value={password}
                                        placeholder="Enter password"
                                    />

                                    <p className="text-danger text-center mt-1">
                                        {showError && errors.passError && errors.passError.length > 0 ?
                                            errors.passError
                                            :
                                            undefined
                                        }
                                    </p>
                                </div>

                                <div className="mb-3">

                                    <input type="password"
                                        className="form-control"
                                        name="cpassword"
                                        id="confirmPass"
                                        onChange={this.handleChange}
                                        value={cpassword}
                                        placeholder="Confirm password"
                                    />

                                    <p className="text-danger text-center mt-1">
                                        {showError && errors.cpassError && errors.cpassError.length > 0 ?
                                            errors.cpassError
                                            :
                                            undefined
                                        }
                                        {showError && errors.cpassError && errors.cpassError.length > 0 ? undefined : errors.matchPassword && errors.matchPassword.length > 0 ?
                                            errors.matchPassword
                                            :
                                            undefined
                                        }

                                    </p>

                                </div>


                                <div className="form-check">
                                    <input
                                        name="termsaccepted"
                                        className="form-check-input"
                                        type="checkbox"
                                        onChange={this.handleChange}
                                        value={this.state.termsaccepted}
                                        id="flexCheckChecked"
                                    />
                                    <label className="form-check-label text-secondary font" for="flexCheckChecked">
                                        I accept the Terms and Conditions
                                    </label>
                                    <p className="text-danger text-center mb-2 mt-1">
                                        {showError && errors.termsAcceptedError && errors.termsAcceptedError.length > 0 ?
                                            errors.termsAcceptedError
                                            :
                                            undefined
                                        }
                                    </p>
                                </div>
                                <button type="submit" className="btn btn-primary w-100 mt-2" onClick={this.handleSubmit}>Sign <span>up</span></button>
                                <div className='mt-2 mb-2'>
                                    <p className='text-center text-secondary'>---or---</p>
                                </div>
                                <div className='d-flex justify-content-evenly sign-g'>
                                    <div className='w-50'>
                                        <Link to="/login">
                                            <button className='btn btn-primary w-75 '>
                                                Sign <span>in</span>
                                            </button>
                                        </Link>

                                    </div>
                                    <div className='w-50'>
                                        <GoogleLogin
                                            clientId={clientId}
                                            buttonText="Sign in"
                                            onSuccess={this.loginSuccessWithGoogle}
                                            onFailure={this.loginFailureWithGoogle}

                                            cookiePolicy={'single_host_origin'}
                                        />
                                    </div>


                                </div>

                            </form>

                        </div>

                    </div>

            }


        </div>;
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Signup);
