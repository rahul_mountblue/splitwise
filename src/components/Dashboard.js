import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { userData, addFriend, addExpenses } from '../redux/action/userActions';

const mapStateToProps = (props) => {
    return {
        user: props.allUser,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        userData: (data) => dispatch(userData(data)),
        addFriend: (data) => dispatch(addFriend(data)),
        addExpenses:(data)=> dispatch(addExpenses(data)),

    }
}


class Dashboard extends Component {
    constructor(props) {
        super(props)
        this.state = {

            showDashBoard: false,
            showActivity: false,
            showAddExpenses: false,
            show: false,
            showExpensesModel: false,
            freinds: [],
            name: '',
            names: '',
            amount: null,
            description: '',
        }
    }
    handleChange = (event) => {

        this.setState({ [event.target.name]: event.target.value })

    }

    handleSubmit = () => {
        //   console.log(this.state.name);
        this.props.addFriend(this.state.name);
        this.handleClose();
    }

    handleShow = () => {
        this.setState({ show: true });
    }

    handleClose = () => {
        this.setState({ show: false });
    }

    handleAddExpenses = () => {
        this.setState({ showExpensesModel: true });
    }

    handleExpense = () => {
        this.setState({ showExpensesModel: false });
    }
    handleExpenseSubmit = (event) => {
        event.preventDefault();
        let expensdata = {};
        expensdata.name = this.state.names;
        expensdata.amount = this.state.amount;
        expensdata.description = this.state.description;
        console.log(expensdata);
        this.props.addExpenses(expensdata);
        this.handleExpense();

    }
    expenseModelChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });

    }

    render() {
          console.log(this.props.user.expensesData)
          const {expensesData}=this.props.user;
          let value=expensesData.reduce((acc,curr)=>{
            return acc+Number(curr.amount)/2;
          },0)
          
        const { show } = this.state;
        return <div className='container shadow-sm border'>
            <div className='row p-1 '>
                <div className='border bg-light d-flex justify-content-between p-2'>
                    <h1 className='h5 mt-1'>DashBoard</h1>
                    <div>
                        <button className='btn btn-warning m-1' onClick={this.handleAddExpenses}>
                            Add an expenses
                        </button >
                        <button className="btn btn-info m-1">
                            Settle up
                        </button>
                    </div>


                </div>
                <div className='d-flex flex-row justify-content-between p-2 border-bottom'>
                    <div className='d-flex flex-column align-items-center'>
                        <span className='text-muted'>Total Balance</span>
                        <span>+ ₹{value}.00</span>
                    </div>
                    <div className='d-flex flex-column align-items-center'>
                        <span className='text-muted'>You Owe</span>
                        <span>₹0.00</span>
                    </div>
                </div>
            
                <div className='text-center mt-4'>
                    <h1 className='h3'>Let's get started!</h1>
                    <h2 className='h5'>
                        To add a new expense, click on “Add an expense” button.
                    </h2>
                    {/* <button className='btn btn-info mb-3' onClick={this.handleShow}>Add friend</button> */}
                </div>
                {/* <Modal show={show}>
                    <Modal.Header >
                        <Modal.Title>Add friend</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form>
                            <div class="form-group mt-2">

                                <input type="text"
                                    name='name'
                                    className="form-control"
                                    id="exampleInputEmail1"
                                    aria-describedby="emailHelp"
                                    onChange={this.handleChange}
                                    placeholder="Enter name"
                                />
                            </div>

                            <div class="form-group mt-2">

                                <input type="email"
                                    name="email"
                                    className="form-control"
                                    id="exampleInputPassword1"
                                    placeholder="Enter email"
                                    onChange={this.handleChange}
                                />
                            </div>

                        </form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.handleClose}>
                            Close
                        </Button>
                        <Button variant="primary" onClick={this.handleSubmit}>
                            Add
                        </Button>
                    </Modal.Footer>
                </Modal> */}
            </div>

            <Modal show={this.state.showExpensesModel}>
                <Modal.Header >
                    <Modal.Title>Add an expenses</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form>
                        <div class="form-group mt-2">
                            <label htmlFor="" className="mb-1">
                                With you and
                            </label>
                            <input type="text"
                                name='names'
                                className="form-control"
                                id="exampleInputEmail1"
                                aria-describedby="emailHelp"
                                onChange={this.expenseModelChange}
                                placeholder="Enter name"
                            />
                        </div>
                        <div class="form-group mt-2">
                            <label htmlFor="" className="mb-1">
                                Total bill
                            </label>
                            <input type="number"
                                name='amount'
                                className="form-control"
                                id="exampleInputEmail1"
                                aria-describedby="emailHelp"
                                onChange={this.expenseModelChange}
                                placeholder="Enter amount"
                            />
                        </div>

                        <div className="form-group mt-2">
                            <label for="exampleFormControlTextarea1" className="mb-1">Add Description</label>
                            <textarea

                                className="form-control"
                                id="exampleFormControlTextarea1"
                                rows="3"
                                name="description"
                                onChange={this.expenseModelChange}
                            >

                            </textarea>
                        </div>

                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={this.handleExpense}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={this.handleExpenseSubmit}>
                        Add
                    </Button>
                </Modal.Footer>
            </Modal>

        </div>;
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
