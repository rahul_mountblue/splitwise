import React, { Component } from 'react';
import Asset1 from '../image/asset1.png'
import Asset2 from '../image/asset2.png'
import Asset3 from '../image/asset3.png'
import Asset4 from '../image/asset4.png'
import Asset5 from '../image/asset5.png'
import Icon1 from '../icons/core-feature.svg';
import Icon2 from '../icons/pro-feature.svg'

export default class Banner extends Component {
    render() {
        return <div className='conatiner-fluid '>
            <div className="container-fluid bg-setup">
                <div className="row">
                    <div className="col-sm-6 text-white  banner11 d-flex flex-column  text-center justify-content-center align-items-center">
                        <div>
                            <h1 className='h2 mt-3'> Track balances</h1>
                            <h3 className='set-width h6'> Keep track of shared expenses, balances, and who owes who.</h3>
                        </div>
                        <div>
                            <img src={Asset1} alt="mobile" />
                        </div>

                    </div>
                    <div className="col-sm-6 text-white  banner12 d-flex flex-column text-center justify-content-center align-items-center">
                        <div>
                            <h1 className='h2 p-2 mt-3'>Organize expenses</h1>
                            <h3 className='set-width h6'> Split expenses with any group: trips, housemates, friends, and family.</h3>
                        </div>
                        <div>
                            <img src={Asset2} alt="mobile" />
                        </div>
                    </div>
                </div>

            </div>
            <div className="container-fluid bg-setup">
                <div className="row ">
                    <div className="col-sm-6 text-white  opacity-75 banner21 d-flex flex-column  text-center justify-content-center align-items-center">
                        <div>
                            <h1 className='h2 p-2 mt-3'>Add expenses easily</h1>
                            <h3 className='set-width h6'> Quickly add expenses on the go before you forget who paid.</h3>
                        </div>
                        <div>
                            <img src={Asset3} alt="mobile" />
                        </div>

                    </div>
                    <div className="col-sm-6 text-white  opacity-75 banner22 d-flex flex-column  text-center justify-content-center align-items-center">
                        <div>
                            <h1 className='h2 p-2 mt-3'>Pay friends back</h1>
                            <h3 className='set-width h6'>Settle up with a friend and record any cash or online payment.</h3>
                        </div>
                        <div>
                            <img src={Asset4} alt="mobile" />
                        </div>
                    </div>

                </div>
            </div>
            <div className="container-fluid bg-setup">
                <div className="row justify-content-center align-items-center banner3 opacity-75 text-white text-center">
                    <div className="col-sm-6 justify-content-center align-items-center">
                        <div className="d-flex flex-column justify-content-center align-items-center">
                            <h1 className='h3'>Get even more with PRO</h1>
                            <h4 className="set-width">
                                Get even more organized with receipt scanning, charts and graphs, currency conversion, and more!
                            </h4>
                            <button className='btn btn-secondary'>Sign <span>up</span></button>
                        </div>

                    </div>
                    <div className="col-sm-6 d-flex justify-content-center align-item-center mt-5 ">
                        <div>

                            <img src={Asset5} alt="asset-5" />
                        </div>
                    </div>
                </div>
            </div>
            <div className='container mx-auto '>
                <div>
                    <h1 className='h1 text-center p-4 mt-2'>The whole nine yards</h1>
                </div>
                <div className='row p-3'>
                    <div className='col-sm-4'>
                        <ul className='li-padding'>
                            <li>
                                <img src={Icon1} alt="" /> <span>Add groups and friends</span>
                            </li>
                            <li>
                                <img src={Icon1} alt="" /> <span>Split expenses, record debts</span>
                            </li>
                            <li>
                                <img src={Icon1} alt="" /> <span>Equal or unequal splits</span>
                            </li>
                            <li>
                                <img src={Icon1} alt="" /> <span>Split by % or shares</span>
                            </li>

                            <li>
                                <img src={Icon1} alt="" /> <span>Calculate total balances</span>
                            </li>
                            <li>
                                <img src={Icon1} alt="" /> <span>Suggested repayments</span>
                            </li>
                            <li>
                                <img src={Icon1} alt="" /> <span>Simplify debts</span>
                            </li>
                            <li>
                                <img src={Icon1} alt="" /> <span>Recurring expenses</span>
                            </li>
                        </ul>
                    </div>

                    <div className='col-sm-4'>
                        <ul className='li-padding'>


                            <li >
                                <img src={Icon1} alt="" /> <span>Offline mode</span>
                            </li>
                            <li>
                                <img src={Icon1} alt="" /> <span>Cloud sync</span>
                            </li>
                            <li>
                                <img src={Icon1} alt="" /> <span>Spending totals</span>
                            </li>
                            <li>
                                <img src={Icon1} alt="" /> <span>Categorize expenses</span>
                            </li>
                            <li>
                                <img src={Icon1} alt="" /> <span>
                                    Easy CSV exports</span>
                            </li>
                            <li>
                                <img src={Icon1} alt="" /> <span>7+ languages</span>
                            </li>
                            <li>
                                <img src={Icon1} alt="" /> <span>100+ currencies</span>
                            </li>
                            <li>
                                <img src={Icon1} alt="" /> <span>Payment integrations</span>
                            </li>
                        </ul>
                    </div>
                    <div className='col-sm-4'>
                        <ul className='li-padding'>
                            <li>
                                <img src={Icon2} alt="" /> <span>A totally ad-free experience</span>
                            </li>
                            <li>
                                <img src={Icon2} alt="" /> <span>
                                    Currency conversion</span>
                            </li>
                            <li>
                                <img src={Icon2} alt="" /> <span>Receipt scanning</span>
                            </li>
                            <li>
                                <img src={Icon2} alt="" /> <span>Itemization</span>
                            </li>
                            <li>
                                <img src={Icon2} alt="" /> <span>Charts and graphs</span>
                            </li>
                            <li>
                                <img src={Icon2} alt="" /> <span>
                                    Expense search</span>
                            </li>
                            <li>
                                <img src={Icon2} alt="" /> <span>
                                    Save default splits</span>
                            </li>
                            <li>
                                <img src={Icon2} alt="" /> <span>Early access to new features</span>
                            </li>
                        </ul>

                    </div>

                </div>

            </div>
            <div className='conatiner d-flex justify-content-center'>
                <div className='col-sm-2'>
                    <ul>
                        <li >
                            <img src={Icon1} alt="" /> <span>Core features</span>
                        </li>
                    </ul>
                </div>
                <div className='col-sm-2'>
                    <ul>
                        <li >
                            <img src={Icon2} alt="" /> <span>Pro features</span>
                        </li>
                    </ul>
                </div>

            </div>

        </div>;
    }
}
