import React, { Component } from 'react';
import Splitewise from '../image/splitwiseIcon.svg';
import Main from './Main';
import {Link} from 'react-router-dom';
import Addgroup from './Addgroup';

class Welcome extends Component {
    constructor(props){
        super(props);
        this.state = {
            showmain:false,
        }
    }
    handleClick = ()=>{
        this.setState({showmain:true});

    }
    render() {
        return <div className='container mt-3'>
            {
            this.state.showmain ?

                   <Main />
            :
            <div className="row justify-content-center align-items-center">
                <div className='col-sm-6 d-flex flex-column justify-content-center align-items-center'>
                    <div>
                        <img src={Splitewise} alt="splitwise-icon" />
                        <span className='icon-span'>
                            Welcome to Splitwise!
                        </span>
                    </div>
                    <h1 className='h5 mt-1 text-info'>
                        What would you like to do first?
                    </h1>
                    <div className='d-flex flex-column  p-2'>
                        <div className='btn btn-primary mt-2 d-flex flex-row align-items-center'>
                            <i className="fa fa-home" style={{ fontSize: "30px", color: "white",marginRight: "10px" }}></i>
                            <span>Add your apartment</span>
                        </div>
                        <div className='btn btn-danger mt-2 d-flex flex-row align-items-center'>
                            <Link to="/addgroup"> 
                            <i className="fa fa-globe" style={{ fontSize: "30px", color: "white", marginRight: "10px" }}></i>
                            <span className='text-light'>
                                Add a group trip
                            </span>
                            </Link>
                        </div>

                        <button className='btn btn-secondary mt-2' onClick={this.handleClick}>
                            <span>
                                Skip setup for now
                            </span>

                        </button>
                    </div>

                </div>

            </div>
    }
        </div>;
    }
}

export default Welcome;
