import React, { Component } from 'react';
import { connect } from 'react-redux';
import { userData, addFriend, addExpenses } from '../redux/action/userActions';

const mapStateToProps = (props) => {
    return {
        user: props.allUser,
    }
}

class Allexpenses extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        console.log(this.props.user.expensesData);
        return <div className='container'>
            <div className="row border">
                <div className='border bg-light d-flex justify-content-between p-2'>
                    <h1 className='h5'>All expenses</h1>
                </div>
                <div>
                    {this.props.user.expensesData.length > 0 ?
                        this.props.user.expensesData.map((data) => {
                            return <div className='d-flex flex-row justify-content-between align-items-center p-3 border mt-1 mb-1 pointer singleExpense'>
                                <h1 className='pointer description'>{data.description}</h1>

                                <div className='d-flex flex-column align-items-center'>
                                    <span className='text-muted' style={{ fontSize: "12px" }}>you paid</span>
                                    <span className='amount'>₹{data.amount}</span>
                                </div>
                                <div className='d-flex flex-column align-items-center'>
                                    <span className='text-muted' style={{ fontSize: "12px" }}>you owed {data.name}</span>
                                    <span className='amount'>₹{data.amount / 2}</span>
                                </div>
                            </div>

                        })
                        :
                        undefined
                    }
                </div>
            </div>

        </div>;
    }
}

export default connect(mapStateToProps)(Allexpenses);
