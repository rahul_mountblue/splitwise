import React, { Component } from 'react';
import { connect } from 'react-redux';
import { userData,login } from '../redux/action/userActions';


const mapStateToProps = (props) => {
    return {
        user: props.allUser,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        userData: (data) => dispatch(userData(data)),
        login : (data)=> dispatch(login(data)),

    }
}


class RecentActivity extends Component {
 constructor(props) {
     super(props)

 }
  render() {
      console.log(this.props.user.expensesData)
    return <div className='d-flex flex-column p-2' style={{gap:"10px"}}>
            {
                this.props.user.expensesData.length===0?<div>No recent activities</div>:this.props.user.expensesData.map((ele)=>{
                    return (<div className='d-flex flex-column border shadow-sm p-3'style={{gap:"5px"}}>
                        <div>You added <span className='amount'>"{ele.description}"</span> </div>
                        <div className="activity-amount">You get back ₹{ele.amount/2}</div>
                    </div>)
                })
            }
    </div>;
  }
}

export default connect(mapStateToProps,mapDispatchToProps) (RecentActivity);
