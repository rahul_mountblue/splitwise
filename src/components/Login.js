import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userData, login } from '../redux/action/userActions';
import Welcome from './Welcome';
import Header from './Header';


const mapStateToProps = (props) => {
    return {
        user: props.allUser,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        userData: (data) => dispatch(userData(data)),
        login: (data) => dispatch(login(data)),

    }
}

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            error: '',
            showError: false,
        }
    }
    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value })
        this.setState({showError:false})
    }
    handleSubmit = (event) => {
        event.preventDefault();
        let user = this.props.user.userData;
        if (this.state.email === user.email && this.state.password === user.password) {
            this.props.login(true);
        } else {
            this.setState({
                error: 'Invalid email or password',
                showError: true,
            })
        }
    }

    render() {
        console.log(this.props.user.login)
        return (
            <div className='container-fluid'>
                <Header></Header>
                {this.state.showError ?
                    <div className='show-error'>
                         <p className='text-danger text-center p-3'>{this.state.error}</p>
                    </div>
                    :
                    undefined
                }
                {
                    this.props.user.login ?

                        <Welcome />
                        :
                        <div className="row justify-content-center align-items-center mt-5">
                            <div className="col-sm-4 col-md-3">
                                <form className='card p-3'>
                                    <h1 className="text-center text-info h4">Sign <span>in</span></h1>
                                    <div class="form-group mt-2">

                                        <label for="exampleInputEmail1" className='mb-1'>Email address</label>
                                        <input type="email"
                                            name='email'
                                            onChange={this.handleChange}
                                            className="form-control"
                                            id="exampleInputEmail1"
                                            aria-describedby="emailHelp"
                                            placeholder="Email"
                                        />

                                    </div>
                                    <div className="form-group mt-2">

                                        <label for="exampleInputPassword1" className='mb-1'>Password</label>
                                        <input type="password"
                                            name='password'
                                            onChange={this.handleChange}
                                            className="form-control"
                                            id="exampleInputPassword1"
                                            placeholder="Password"
                                        />
                                    </div>

                                    <button type="submit" className="btn btn-primary mt-3 mb-3" onClick={this.handleSubmit}>Sign <span>in</span> </button>
                                    <div>
                                        <p className='text-center text-secondary mb-2'> Don't have an acoount <Link to="/signup">Sign Up</Link></p>



                                    </div>
                                </form>
                            </div>
                        </div>
                }

            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);