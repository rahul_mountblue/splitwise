import React, { Component } from 'react';
import Icon2 from '../icons/pro-feature.svg'
import GoogleAppStore from '../image/googleApp1.png';
import AppleStore from '../image/appleappstore1.png'


class Footer extends Component {
    render() {
        return <div className='container-fluid '>
            <div className='container mt-5'>
            <div className="row">
                <div className="col-sm-2">
                    <ul className='pd-1'>
                        <li className='li-text-color'>Splitwise</li>
                        <li>About</li>
                        <li>Press</li>
                        <li>Blog</li>
                        <li>Jobs</li>
                        <li>Calculators</li>
                        <li>API</li>
                    </ul>
                </div>
                <div className="col-sm-2 justify-content-center">
                    <ul className='pd-1'>
                        <li className='li-text-color-2'>Account</li>
                        <li>Log in</li>
                        <li>Sign up</li>
                        <li>Reset password</li>
                        <li>Settings</li>
                        <li>
                            <img src={Icon2} alt="" /> <span>
                                Splitwise Pro </span>
                        </li>
                    </ul>
                </div>
                <div className="col-sm-2">
                    <ul className='pd-1'>
                        <li className='li-text-color-3'>More</li>
                        <li>Contact us</li>
                        <li>FAQ</li>
                        <li>Terms of Service</li>
                        <li>Privacy Policy</li>
                        <li className='gap-2'>
                            <i class="fa fa-twitter-square" style={{ fontSize: "24px" }}></i>
                            <i className="fa fa-facebook-square" style={{ fontSize: "24px", marginLeft: "5px" }}></i>

                            <i class="fa fa-linkedin-square" style={{ fontSize: "24px", marginLeft: "5px" }}></i>
                        </li>
                    </ul>

                </div>
                <div className="col-sm-6 d-flex flex-column justify-content-center align-items-center">
                    <div className='flex-row'>
                        <img src={GoogleAppStore} alt="google-app-store-icon" className='set-h-w' />
                        <img src={AppleStore} alt="apple-store-icon" className='set-h-w' />
                    </div>
                    <div>
                        <p className='text-dark'>Created by Abset Pvt. Ltd.</p>
                    </div>


                </div>


            </div>
            </div>
            <div className='container-fluid'>
                <svg xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" viewBox="0 0 680 91" class="w-full">
                    <path fill="#ACE4D6" d="M349 76.499L286 113V40z"></path>
                    <path fill="#0C3C32" d="M480 74.5L446 94V55z"></path>
                    <path fill="#1CC29F" d="M223 76.5l63 36.5V40zm182 1.999L446 102V55z"></path>
                    <path fill="#137863" d="M169 48v82l71-41z"></path>
                    <path fill="#1CC29F" d="M121 75.499L169 103V48z"></path>
                    <path fill="#373B3F" d="M456 101h-96V46z"></path>
                    <path fill="#52595F" d="M360 46v55h-96z"></path>
                    <path fill="#A473DB" d="M436 93h63V57z"></path>
                    <path fill="#D0B3EB" d="M499 57v36h63z"></path>
                    <path fill="#0C3C32" d="M491 93h84.18V44z"></path>
                    <path fill="#1CC29F" d="M575.18 93h84.179l-84.18-49z"></path>
                    <path fill="#FF2900" d="M601 94h48V66z"></path>
                    <path fill="#FF692C" d="M649 66v28h48z"></path>
                    <path fill="#FF815C" d="M170.385 93h76V49z"></path>
                    <path fill="#FF2900" d="M246.385 49v44h76z"></path>
                    <path fill="#373B3F" d="M166 93H70V38z"></path>
                    <path fill="#52595F" d="M70 38v55h-96z"></path>
                </svg>

            </div>

        </div>;
    }
}

export default Footer;