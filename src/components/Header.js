import React, { Component } from 'react';
import { GoogleLogout } from 'react-google-login';
import { connect } from 'react-redux';
import { userData, logOut } from '../redux/action/userActions';
import SplitIcon from '../image/splitwiseIcon.svg'

const mapStateToProps = (props) => {
    return {
        user: props.allUser,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        userData: (data) => dispatch(userData(data)),
        logOut: (data) => dispatch(logOut(data)),

    }
}


class Header extends Component {
    constructor(props) {
        super(props)
    }
    logout = () => {
        this.props.logOut(false);
        this.setState({ login: false });
    }

    render() {

        return <div className='container-fluid'>
            <nav className="navbar navbar-light bg-header w-100">

                <div className="navbar-brand" style={{ marginLeft: "10px" }}>
                    <img src={SplitIcon} alt="splitwise" className='img-fluid img-icon' />
                    <span className='split'>Splitwise</span>

                </div>
                <div className="d-flex flex-row align-items-center" style={{ marginRight: "10px", gap: "5px" }}>
                    {
                        this.props.user.login ?
                            <div className='d-flex flex-row align-items-center' >
                                <div className='p-1'>

                                    <i className="fa fa-user-circle-o" style={{ fontSize: "20px",color:'#f07344' }}></i>

                                </div>
                                <div className='name-color'>
                                    <span>{this.props.user.userData.name}</span>
                                </div>
                            </div>

                            :
                            undefined
                    }

                    {
                        this.props.user.login ?
                            <div className=''>
                                <button className='btn pointer logout' onClick={this.logout}>
                                    Logout
                                </button>

                                {/* <GoogleLogout
                                    clientId='277972961938-euq76op4cdmbujch98ntt100qsslgja1.apps.googleusercontent.com'
                                    buttonText="Logout"
                                    onLogoutSuccess={this.logout}

                                >
                                </GoogleLogout> */}
                            </div>
                            :
                            undefined
                    }

                </div>
            </nav>


        </div>;
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
