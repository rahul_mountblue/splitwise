import React, { Component } from 'react';
import { Routes, Route } from 'react-router-dom';
import Signup from './components/Signup';
import Home from './components/Home'
import Footer from './components/Footer';
import Login from './components/Login';
import Addgroup from './components/Addgroup';

class App extends Component {
  render() {
    return <div className='container-fluid'>
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="/signup" element={<Signup />} />
        <Route path="/login" element={<Login />} />
        <Route path="/addgroup" element={<Addgroup />} />
      </Routes>
      <Footer />
    </div>
  }
}

export default App;
